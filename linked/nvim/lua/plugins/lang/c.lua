local vim = vim
return {
  -- Treesitter parsers
  {
    "nvim-treesitter/nvim-treesitter",
    ft="c",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        "c",
      })
    end,
  },
  -- LSP/Formatting/Linting binaries via Mason
  {
    "williamboman/mason.nvim",
    ft="c",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        "clang-format",
      })
    end,
  },
  -- LSP server configuration
  {
    "williamboman/mason-lspconfig.nvim",
    ft="c",
    opts = {
      servers = {
        clangd = {
          capabilities = {
            offsetEncoding = "utf-8",
          },
          settings = {},
        },
      },
    },
  },
}

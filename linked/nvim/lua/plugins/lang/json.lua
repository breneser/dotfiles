return {
  -- Treesitter parsers
  {
    "nvim-treesitter/nvim-treesitter",
    ft = { "json", "jsonc" },
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        "json",
        "jsonc",
      })
    end,
  },
  -- LSP server configuration
  {
    "williamboman/mason-lspconfig.nvim",
    ft = { "json", "jsonc" },
    opts = {
      servers = {
        jsonls = {},
      },
    },
  },
}

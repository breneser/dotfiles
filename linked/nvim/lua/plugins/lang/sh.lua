local vim = vim
return {
  -- Treesitter parsers
  {
    "nvim-treesitter/nvim-treesitter",
    ft = "sh",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        "bash",
      })
    end,
  },
  -- LSP/Formatting/Linting binaries via Mason
  {
    "williamboman/mason.nvim",
    ft = "sh",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        "shfmt",
        "shellcheck",
      })
    end,
  },
  -- Linting configuration via nvim-lint
  {
    "mfussenegger/nvim-lint",
    ft = "sh",
    opts = {
      linters_by_ft = {
        sh = { "shellcheck" },
      },
    },
  },
  -- Formatting configuration via conform
  {
    "stevearc/conform.nvim",
    ft = "sh",
    opts = {
      formatters_by_ft = {
        sh = { "shfmt" },
      },
    },
  },
  -- LSP server configuration
  -- {
  --   "williamboman/mason-lspconfig.nvim",
  --   opts = {
  --     servers = {},
  --   },
  -- },
  -- {
  --   "nvim-neotest/neotest",
  --   ft = { "bash", "sh" },
  --   dependencies = {
  --     "rcasia/neotest-bash",
  --   },
  --   opts = function(_, opts)
  --     vim.list_extend(opts.adapters, {
  --       require("neotest-bash")({
  --         -- Extra arguments for nvim-dap configuration
  --         -- See https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings for values
  --         -- dap = { justMyCode = false },
  --         -- Command line arguments for runner
  --         -- Can also be a function to return dynamic values
  --         args = { "--log-level", "DEBUG" },
  --         -- Runner to use. Will use pytest if available by default.
  --         -- Can be a function to return dynamic value.
  --         runner = "pytest",
  --         -- Custom python path for the runner.
  --         -- Can be a string or a list of strings.
  --         -- Can also be a function to return dynamic value.
  --         -- If not provided, the path will be inferred by checking for
  --         -- virtual envs in the local directory and for Pipenev/Poetry configs
  --         -- python = ".venv/bin/python",
  --         -- Returns if a given file path is a test file.
  --         -- NB: This function is called a lot so don't perform any heavy tasks within it.
  --         -- is_test_file = function(file_path)
  --         --   ...
  --         -- end,
  --         -- !!EXPERIMENTAL!! Enable shelling out to `pytest` to discover test
  --         -- instances for files containing a parametrize mark (default: false)
  --         pytest_discover_instances = true,
  --       }),
  --     })
  --   end,
  -- },
}

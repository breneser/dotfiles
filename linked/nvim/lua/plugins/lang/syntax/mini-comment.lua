return {
  "echasnovski/mini.comment",
  event = "VeryLazy",
  opts = {
    options = {
      custom_commentstring = function()
        return require("ts_context_commentstring.internal").calculate_commentstring() or vim.bo.commentstring
      end,
      -- Whether to ignore blank lines
      ignore_blank_line = false,
      -- Whether to recognize as comment only lines without indent
      start_of_line = false,
      -- Whether to ensure single space pad for comment parts
      pad_comment_parts = true,
    },
    mappings = {
      comment = "gc",

      -- Toggle comment on current line
      comment_line = "gcc",

      -- Define 'comment' textobject (like `dgc` - delete whole comment block)
      textobject = "ac",
    },
  },
}


return {
  "stevearc/conform.nvim",
  cmd = "ConformInfo",
  opts = {
    formatters_by_ft = {},
    format_on_save = {
      -- These options will be passed to conform.format()
      async = false,
      -- Time in milliseconds to block for formatting. No effect if async = true
      timeout_ms = 1000,
      lsp_format = "fallback",
    },
  },
  config = function(_, opts)
    local conform = require("conform")
    vim.o.formatexpr = "v:" .. conform.formatexpr()

    if not require("settings.lsp").autoformat then
      opts.format_on_save = nil
    end

    require("conform").setup(opts)
  end,
}

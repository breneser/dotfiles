-- Markdown shortcuts with which_key
local vim = vim
vim.api.nvim_create_autocmd("FileType", {
  pattern = "markdown, md",
  callback = function()
    local buf = vim.api.nvim_get_current_buf()
    require("which-key").add({
      {
        "<localleader>",
        buffer = buf,
        group = "Markdown",
        nowait = true,
        remap = false,
      },
      {
        "<localleader>b",
        "<cmd>MarkdownPreview<cr>",
        buffer = buf,
        desc = "View in browser",
        nowait = true,
        remap = false,
      },
      {
        "<localleader>v",
        "<cmd>Glow<cr>",
        buffer = buf,
        desc = "Preview markdown",
        nowait = true,
        remap = false,
      },
    })
  end,
})

-- Plugins
return {
  {
    "nvim-treesitter/nvim-treesitter",
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        "markdown",
        "markdown_inline",
      })
    end,
  },
  {
    "iamcco/markdown-preview.nvim",
    build = function()
      vim.fn["mkdp#util#install"]()
    end,
    ft = "markdown",
    config = function()
      vim.g.mkdp_auto_start = 0
      vim.g.mkdp_auto_close = 0
    end,
  },
  {
    "npxbr/glow.nvim",
    ft = "markdown",
    config = function()
      require("glow").setup({
        -- style = "light",
        border = "rounded", -- floating window border config
        pager = false,
        width = 300,
        height = 600,
        width_ratio = 0.8, -- maximum width of the Glow window compared to the nvim window size (overrides `width`)
        height_ratio = 0.7,
      })
    end,
  },
}

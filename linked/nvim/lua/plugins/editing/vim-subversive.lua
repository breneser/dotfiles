return {
  "svermeulen/vim-subversive",
  keys = {
    { "gr", "<Plug>(SubversiveSubstitute)", mode = { "n", "x" } },
    { "grr", "<Plug>(SubversiveSubstituteLine)" },
    { "gR", "<Plug>(SubversiveSubstituteToEndOfLine)" },
  },
}

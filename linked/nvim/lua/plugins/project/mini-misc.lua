return {
  "echasnovski/mini.misc",
  version = false,
  lazy = false,
  config = function()
    require("mini.misc").setup({})
    -- Auto chdir to project root
    MiniMisc.setup_auto_root({
      ".git",
      -- "Makefile",
      -- "package.json",
      -- "go.mod",
    })
  end,
}

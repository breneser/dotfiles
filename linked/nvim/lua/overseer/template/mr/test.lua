return {
	name = "mr test",
	builder = function()
		return {
			cmd = { "mr" },
			args = { "test" },
		}
	end,
	desc = "Test project",
	priority = 10,
	condition = {
		dir = "~/projects",
	},
}

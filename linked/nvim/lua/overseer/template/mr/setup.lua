return {
	name = "mr setup",
	builder = function()
		return {
			cmd = { "mr" },
			args = { "setup" },
		}
	end,
	desc = "Setup/Reset initial project state",
	priority = 50,
	condition = {
		dir = "~/projects",
	},
}

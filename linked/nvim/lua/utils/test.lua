local M = {}

function M.run_test()
  require("neotest").run.run()
end

function M.debug_test()
  require("neotest").run.run({ strategy = "dap" })
end

function M.run_buffer_tests()
  require("neotest").run.run(vim.fn.expand("%"))
end

function M.run_dir_tests()
  require("neotest").run.run(vim.fn.expand("%:h"))
end

function M.toggle_summary()
  require("neotest").summary.toggle()
end

function M.open_output()
  require("neotest").output.open()
end

function M.open_enter_output()
  require("neotest").output.open({ enter = true })
end

function M.attach_test_output()
  require("neotest").run.attach()
end

function M.stop()
  require("neotest").run.stop()
end

return M

local obj = {}

-- Adapted from AppLauncher in  "https://github.com/Hammerspoon/Spoons"
obj.name = "Launcher"

--- Lancher.modifiers
--- Variable
--- Modifier keys used when launching apps
---
--- Default value: `{"ctrl", "alt"}`
obj.modifiers = { "ctrl", "alt" }
obj.hotkeys = {}

local launchNew = function(appName, opts)
	local cmd = "open --new " .. (opts.bundleID and "-b " or "-a ") .. appName
	print("Launching " .. cmd)
	hs.execute(cmd)
end

local toggleApp = function(appName, opts)
	print("Toggling " .. appName)
	local instance = hs.application.get(appName)
	print("Instance")
	print(instance)
	if not instance then
		launchNew(appName, opts)
		return
	end

	if instance:isFrontmost() then
		instance:hide()
	else
		instance:activate()
	end
end

local launchApp = function(appName, opts)
	print("Launching " .. appName)
	opts = opts or {}
	setmetatable(opts, {
		__index = {
			single = false,
			bundleID = false,
		},
	})

	if opts.single then
		toggleApp(appName, opts)
	else
		launchNew(appName, opts)
	end
end

--- Lancher:bind(mapping)
--- Method
--- Binds hotkeys for Lancher
---
--- Parameters:
---  * mapping - A table containing single characters with their associated app
function obj:bind(mapping)
	for key, config in pairs(mapping) do
		local appName = config
		local opts
		if type(config) == "table" then
			appName = config[1]
			opts = config
		end
		hs.hotkey.bind(self.modifiers, key, function()
			launchApp(appName, opts)
		end)
	end
end

function obj:use(config)
	setmetatable(config, {
		__index = {
			modifiers = obj.modifiers,
			hotkeys = obj.hotkeys,
		},
	})
	self.modifiers = config.modifiers
	self.hotkeys = config.hotkeys
	self:bind(config.hotkeys)
end

return obj

local wezterm = require("wezterm")

local function is_vim(pane)
	return pane:get_user_vars().IS_NVIM == "true"
end

-- local function focus(direction)
-- 	return runner()
-- end
--
-- local function skip(direction)
-- 	return runner({ AdjustPaneSize = { direction, 3 } })
-- end

local function skip(keyconf)
	return {
		key = keyconf.key,
		mods = keyconf.mods,
		action = wezterm.action_callback(function(win, pane)
			if is_vim(pane) then
				-- pass the keys through to vim/nvim
				win:perform_action({ SendKey = { key = keyconf.key, mods = keyconf.mods } }, pane)
			else
				win:perform_action(keyconf.action, pane)
			end
		end),
	}
end

return {
	skip = skip,
}
